<?php
error_reporting(E_ALL);

class Verification
{
    protected $dbFile = 'password-db';
    protected $rootDir = __DIR__;
    protected $databaseShema = array();

    protected $dbPath;

    function __construct()
    {
        $this->checkDBFile();
    }

    public function ajaxCheckKey()
    {
        $result = false;

        if (isset($_POST['password'])) {
            $password = $this->adman_decoded($_POST['password']);

            if ($this->checkKeyDB($password)) {
                if(isset($this->getDB($password)[$password]))
                $result = true;
            }
        }

        return json_encode(array(
            'result' => $result
        ));
    }

    public function ajaxAddItem()
    {
        $result = false;

        if (isset($_POST['password'])) {
            $password = $_POST['password'];
            if ($this->checkKeyDB($password) != $password) {
                if ($this->setDB($password, true)) {
                    $result = true;
                }
            }
        }

        return json_encode(array(
            'result' => $result
        ));
    }

    public function ajaxListItems()
    {
        $db = unserialize($this->adman_decoded(file_get_contents($this->dbPath)));
        return json_encode(array(
            'data' => $db
        ));
    }

    public function ajaxRemoveItem()
    {
        $result = false;
        if (isset($_POST['password'])) {
            $password = $_POST['password'];
            if ($this->removeKeyDB($password)) {
                $result = true;
            }
        }
        return json_encode(array(
            'result' => $result
        ));
    }

    public function ajaxDisablingKey()
    {
        $result = false;
        if (isset($_POST['password'])) {
            $password = $this->adman_decoded($_POST['password']);
            if ($this->checkKeyDB($password)) {
                if ($this->setDB($password, false)){
                    $result = true;
                }
            }
        }
        return json_encode(array(
            'result' => $result
        ));
    }

    private function checkDBFile()
    {
        $DS = DIRECTORY_SEPARATOR;
        $this->dbPath = $this->rootDir . $DS . $this->dbFile;
        if (!file_exists($this->dbPath)) {
            if (!file_put_contents($this->dbPath, $this->adman_encoded(serialize($this->databaseShema)))) {
                throw new Exception('Error creating and writing the database file');
            }
        }

        return $this->dbPath;
    }

    private function setDB($key, $value)
    {
        $db = unserialize($this->adman_decoded(file_get_contents($this->dbPath)));
        if ((isset($key))) {
            $db[$key] = $value;
            if ($this->saveDB($db))
                return true;
        }
        return false;
    }

    private function checkKeyDB($key)
    {
        $db = unserialize($this->adman_decoded(file_get_contents($this->dbPath)));

        return (isset($db[$key])) ? $db[$key] : false;
    }

    private function getDB($key)
    {
        $db = unserialize($this->adman_decoded(file_get_contents($this->dbPath)));

        if ($key === "all")
            return $db;

        return (isset($db[$key])) ? [$key => $db[$key]] : false;
    }

    private function removeKeyDB($key)
    {
        $db = unserialize($this->adman_decoded(file_get_contents($this->dbPath)));

        if (isset($db[$key])) {
            unset($db[$key]);
            $this->saveDB($db);
            return true;
        }

        return false;
    }

    private function saveDB($data)
    {
        if (!file_put_contents($this->dbPath, $this->adman_encoded(serialize($data)))) {
            throw new Exception('Error creating and writing the database file');
        }
        return true;
    }

    private function adman_encoded($word)
    {
        return base64_encode($word);
    }

    private function adman_decoded($data)
    {
        return base64_decode($data);
    }

}


$app = new Verification();

if (isset($_GET['ajaxAddItem'])) {
    echo $app->ajaxAddItem();
    exit;
}

if (isset($_GET['ajaxListItems'])) {
    echo $app->ajaxListItems();
    exit;
}

if (isset($_GET['ajaxRemoveItem'])) {
    echo $app->ajaxRemoveItem();
    exit;
}

if (isset($_GET['checkKey'])) {
    echo $app->ajaxCheckKey();
    exit;
}

if (isset($_GET['disablingKey'])) {
    echo $app->ajaxDisablingKey();
    exit;
}


?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0">
    <title>Adman</title>
    <link rel="shortcut icon"
          href="data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAuLi4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAREAAAAAAAARERAAAAAREBEREQAAARAQAREREAARARAAERERARARAAABERARARAAAAARERARAAAAAAABEBAAAAAAAQARAAAAABERAAEQAAABERERABEAABERERAAAREAEAEREAABERAAABEQAAEREAAAEQAAABERAAEQAAAAABCP/wAAB/EAAAPlAACByQAAwJMAAOEnAADwTwAA/l8AAPs/AADDnwAAgM8AAAHjAABh4QAA8eEAAPPwAADn/QAA"/>
    <style>
        * {
            margin: 0;
            padding: 0
        }

        li {
            list-style-type: none;
        }

        li:last-child {
            margin-bottom: 0;
        }


        ul {
            margin-left: 0;
            padding-left: 0
        }


        body {
            margin: 0;
            font-family: Roboto, sans-serif;
            font-size: .9rem;
            font-weight: 400;
            line-height: 1.6;
            color: #212529;
            text-align: left;
            background-color: #f4f3ef
        }

        .content {
            max-width: 1200px;
            margin: 2% auto;
            padding: 15px;
            background: #fff;
            box-shadow: 0 6px 10px -4px rgba(0, 0, 0, .15) !important
        }

        .fail-list-file span, .fail-list-file div {
            display: inline-block
        }

        .fail-list-file div {
            width: 80%;
            color: #949393;
            font-weight: 100;
            text-overflow: ellipsis
        }

        .fail-list-file span {
            font-weight: 700;
            text-align: right;
            width: 20%;
            color: #e36209
        }

        a {
            color: #0089ff;
            border-bottom: 2px dotted rgba(0, 137, 255, .3) !important;
            text-decoration-line: none
        }

        a:hover {
            color: #0e36ff
        }

        .name-section {
            font-size: 14pt;
            font-weight: 700;
            margin-top: 15px;
            line-height: initial;
        }

        .name-section:first-child {
            margin: 0;
        }

        .form-control {
            display: block;
            width: 100%;
            padding: .375rem .75rem;
            font-size: 1rem;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border-radius: .25rem;
        }

        .btn {
            display: inline-block;
            font-weight: 400;
            line-height: 1.5;
            color: #212529;
            text-align: center;
            text-decoration: none;
            vertical-align: middle;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
            background-color: transparent;
            border: 1px solid transparent;
            padding: .375rem .75rem;
            font-size: 1rem;
            border-radius: .25rem;
        }

        .btn-group-lg > .btn, .btn-lg {
            padding: .5rem 1rem;
            font-size: 1.25rem;
            border-radius: .3rem;
        }

        .btn-primary {
            color: #fff;
            background-color: #0d6efd;
            border-color: #0d6efd;
        }

        [type=button], [type=reset], [type=submit], button {
            -webkit-appearance: button;
        }

        .form-input {
            width: 50%;
            margin: 0 0 15px;
        }


        a:hover {
            color: #fff;
        }

        .list-item {
            display: block;
            border: 1px solid #dedede;
            background: #d3d5d8;
            padding: 12px;
            position: relative;
        }

        .list-item-remove {
            position: absolute;
            right: 10px;
            top: 4px;
            cursor: pointer;
            background: #fd660d;
            color: #fff;
            font-family: 'Helvetica', 'Arial', sans-serif;
            font-size: 1.8em;
            font-weight: bold;
            text-align: center;
            width: 40px;
            height: 40px;
            border-radius: 5px;
        }

        #iconBlock {
            position: absolute;
            right: -8px;
            top: 10px;
            cursor: pointer;
            width: 16px;
            height: 16px;
            background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAAdgAAAHYBTnsmCAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAADASURBVDiNnZKxDcIwEEVfEopQhAlITQESmzBKmAKqTIIiZgkwAhJUFKQiNKHgLCzrgi2fdLL97/9v+2zQowQaoJM8AosRrip+AGegkrwINg8xaEScW1guJocQg052dWMLPF0wVYgFcFfwGzALMYiOPdADgyd7YKcZhIhNvowosQwGxbQVzlqpJS7g7tLy7VEKnJQ6ABPtLhIZvyZnY6R/BitgI/NljAHA1FP3NvEqY+nRApHPaP/EGnj7jiyc2iw+H25EAN+7GhMAAAAASUVORK5CYII=');
        }

        .rel {
            position: relative;
        }

        .status-active, .status-disabled{
            display: inline-block;
            margin-left: 15px;
            padding: 5px;
            background: #fff;
            border-radius: 15px;
        }

        .status-active {
            background: green;
            color: #fff;
        }

        .status-disabled{
            background: #dedede;
            color: #000;
        }


    </style>
</head>
<body>
<div class="content">

    <div class="form-input rel">
        <input type="text" name="password" placeholder="Password" class="form-control" value="" id="password" required>
        <div id="iconBlock"></div>
    </div>

    <button class="btn btn-primary" name="type" value="save" type="" onclick="addItem()">Add password</button>


    <div class="name-section">Password List</div>
    <div id="list">Loading..</div>

</div>

<script type="application/javascript">
    function XmlHttp() {
        var xmlhttp;
        try {
            xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (E) {
                xmlhttp = false;
            }
        }
        if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
            xmlhttp = new XMLHttpRequest();
        }
        return xmlhttp;
    }

    function ajax(param) {
        if (window.XMLHttpRequest) req = new XmlHttp();
        var send = "";
        for (var i in param.data) send += i + "=" + param.data[i] + "&";
        req.open("POST", param.url, true);
        req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        req.send(send);
        req.onreadystatechange = function () {
            if (req.readyState == 4 && req.status == 200) {
                if (param.success) param.success(req.responseText);
            }
        }
    }

    const addItem = function () {
        const passwordInput = document.getElementById('password');
        ajax({
            url: '<?=basename(__FILE__)?>?ajaxAddItem',
            data: {
                password: passwordInput.value || ""
            },
            success: function (data) {
                const res = JSON.parse(data)
                if (res.result) {
                    passwordInput.value = "";
                    loadList();
                } else {
                    alert("Invalid password");
                }
            }
        })
    }


    document.getElementById('iconBlock').addEventListener('click', function () {
        document.getElementById('password').value = gen_password(12);
    }, false);

    const loadList = function () {
        const list = document.getElementById('list');
        ajax({
            url: '<?=basename(__FILE__)?>?ajaxListItems',
            success: function (data) {
                const res = JSON.parse(data)
                const resData = res.data;
                if (Object.keys(resData).length) {
                    list.innerHTML = ""
                    Object.keys(resData).map(function (v, i) {
                        let isActive = 'disabled'
                        if(resData[v])
                            isActive = 'active'
                        let div = document.createElement("div")
                        let remove = document.createElement("div")
                        let status = document.createElement("div")
                        if(resData[v]){
                            status.classList.add('status-active')
                        }else{
                            status.classList.add('status-disabled')
                        }

                        status.innerText = isActive
                        remove.classList.add('list-item-remove')
                        remove.innerText = "X"
                        remove.setAttribute("key", v)
                        remove.addEventListener('click', function () {
                            ajax({
                                url: '<?=basename(__FILE__)?>?ajaxRemoveItem',
                                data: {
                                    password: v
                                },
                                success: function (data) {
                                    const res = JSON.parse(data)
                                    if (res.result) {
                                        div.remove()
                                    } else {
                                        alert("Invalid password");
                                    }
                                }
                            })

                        }, false);
                        div.classList.add('list-item')
                        div.innerText = v;
                        div.appendChild(status)
                        div.appendChild(remove)
                        list.appendChild(div)
                    })
                } else {
                    list.innerText = "Сlean sheet"
                }

            }
        })
    }

    function gen_password(len) {
        var password = "";
        var symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (var i = 0; i < len; i++) {
            password += symbols.charAt(Math.floor(Math.random() * symbols.length));
        }
        return password;
    }


    loadList();


</script>
</body>
</html>