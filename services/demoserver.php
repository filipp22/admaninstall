<?php

if (isset($_GET['integrityCheck'])) {
    $errors = array();
    $errors[] = array(
        'error' => true,
        'message' => 'Error message IntegrityCheck 1'
    );
    $errors[] = array(
        'error' => true,
        'message' => 'Error message IntegrityCheck 2'
    );
    $errors[] = array(
        'error' => true,
        'message' => 'Error message IntegrityCheck 3'
    );
    $errors[] = array(
        'error' => false,
        'message' => 'Error message IntegrityCheck 4'
    );
    echo json_encode($errors);
} elseif(isset($_GET['websitesIntegrityCheck'])) {
    $errors = array();
    $errors[] = array(
        'error' => true,
        'message' => 'Error message WebsitesIntegrityCheck 1'
    );
    $errors[] = array(
        'error' => true,
        'message' => 'Error message WebsitesIntegrityCheck 2'
    );
    $errors[] = array(
        'error' => false,
        'message' => 'Error message WebsitesIntegrityCheck 3'
    );
    echo json_encode($errors);
}