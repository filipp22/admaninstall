<?php
error_reporting(E_ALL);
ini_set("memory_limit", "256M");
set_time_limit(1800);

class AdmanInstall extends ViewAdman
{

    protected $storage = array('url' => 'http://aaliyaharts.com/wp-content/uploads/2021/02/test2.zip', 'hash' => 'ce452f43efd287f7b50df038776d566b');

    protected $tempDir = 'temp-adman';

    protected $dbFile = 'adman-db';

    protected $successRedirectURI = '/admin/index.php?a=settings';

    protected $rootDir = __DIR__;

    protected $urlService = array(
        'passwordCheck' => 'http://admaninstaller.loc/services/verification.php?checkKey',
        'disablingKey' => 'http://admaninstaller.loc/services/verification.php?disablingKey',
        'integrityCheck' => 'http://admaninstaller.loc/services/demoserver.php?integrityCheck',
        'websitesIntegrityCheck' => 'http://admaninstaller.loc/services/demoserver.php?websitesIntegrityCheck'
    );

    protected $databaseShema = array(
        'step' => "INIT",
        'errors' => array(),
        'password' => '',
        'database' => array(
            'host' => 'localhost',
            'login' => '',
            'password' => '',
            'db' => ''
        ),
        'paths' => array(
            'http' => '',
            'php' => '',
            'nice' => ''
        ),
        'isDownloadFile' => false,
        'isExtractFIle' => false,
        'isDBImport' => false,
        'isRemovedFile' => false,
        'filePath' => '',
        'checksumFile' => '',
        'progressDownloadFile' => 0,
    );

    private $privilegesDB = array(
        'SELECT', 'INSERT', 'UPDATE', 'DELETE', 'CREATE', 'DROP', 'RELOAD', 'SHUTDOWN',
        'PROCESS', 'FILE', 'REFERENCES', 'INDEX', 'ALTER', 'SHOW DATABASES', 'SUPER', 'CREATE TEMPORARY TABLES',
        'LOCK TABLES', 'EXECUTE', 'REPLICATION SLAVE', 'REPLICATION CLIENT', 'REPLICATION CLIENT', 'CREATE VIEW',
        'SHOW VIEW', 'CREATE ROUTINE', 'ALTER ROUTINE', 'CREATE USER', 'EVENT', 'TRIGGER', 'CREATE TABLESPACE',
        'CREATE ROLE', 'DROP ROLE'
    );

    protected $publicUrl;

    private $dbPath;

    protected $extensionsCheck = array('curl', 'mysqli', 'date', 'zip', 'hash', 'json', 'mbstring', 'pcre', 'simplexml', 'openssl', 'IonCube Loader');

    protected $minMemoryLimit = 256;

    protected $checkUpTimeLogFile = 60;

    protected $minPHPVersion = '5.6.0';

    protected $cronLogsDir = "/var/cron_logs";

    protected $checkLogFiles = array('core_cron', 'test_db_response', 'test_db_response_time');

    protected $excludeDirs = array('.git', '.idea');

    protected $pathPHPBin;



    function __construct()
    {

        $this->publicUrl = ((!empty($_SERVER['HTTPS'])) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'];

        $this->checkPermissions();

        $this->checkDBFile();

        $this->pathPHPBin = $this->getPHPExecutableFromPath();
    }

    function init()
    {


        switch ($this->getDB("step")) {

            case "INIT":
                $content = $this->stepPassword();
                break;
            case "DOWNLOADFILE":
                $content = $this->stepDownloadFile();
                break;

            case "SETUP":
                $content = $this->stepSetUpSettings();
                break;

            case "CHECK":
                $content = $this->stepCheckCriticalServer();
                break;

            case "INSTALL":
                $content = $this->stepInstall();
                break;

            case "FINISH":
                $content = $this->installationEnd();
                break;
            default:
                $content = "Error Step";
        }


        echo $this->body($content);
    }

    public function ajaxStartDownloadFile()
    {
        return json_encode(array(
            'result' => $this->download($this->storage['url'])
        ));
    }

    public function ajaxProgressFile()
    {
        return json_encode(array(
            'result' => true,
            'progress' => intval($this->getDB("progressDownloadFile"))
        ));
    }

    public function ajaxFinal()
    {

        $DS = DIRECTORY_SEPARATOR;
        $tempDir = $this->rootDir . $DS . $this->tempDir;
        $fileSQL = $this->rootDir . $DS . 'adman.sql';


        if(file_exists($this->getDB('filePath'))){
            unlink($this->getDB('filePath'));
        }

        if(file_exists($fileSQL)){
            unlink($fileSQL);
        }


        $this->disablingPassword($this->getDB("password"));
        if (file_exists($this->dbPath)) {
            unlink($this->dbPath);
        }

        $result = true;
        rmdir($tempDir);
        unlink(__FILE__);


        return json_encode(array(
            'result' => $result,
            'redirect' => $this->successRedirectURI
        ));

    }

    public function ajaxCheckSumFile()
    {
        $file = md5_file($this->getDB('filePath'));
        $this->setDB('checksumFile', $file);
        if (($file == mb_strtolower($this->storage['hash']))) {
            $this->setDB('isDownloadFile', true);
            $this->setDB('step', "SETUP");
        }
        return json_encode(array(
            'result' => ($file == mb_strtolower($this->storage['hash'])) ? true : false,
            'md5_file' => $file
        ));
    }


    public function ajaxCheckInstall()
    {

        $result = "";

        $data = $this->checkIntegrity();

        if (gettype($data) != "boolean") {
            foreach ($data as $item) {
                if (isset($item['error']) && isset($item['message'])) {
                    if ($item['error'])
                        $result = $item['message'];
                }
            }

        } else $result = "Verification server is not available";

        return json_encode(array(
            'result' => $result ? false : true,
            'error' => $result
        ));
    }

    public function ajaxRemovingFile()
    {
        $result = false;
        $DS = DIRECTORY_SEPARATOR;
        $fileSQL = $this->rootDir . $DS . 'adman.sql';

        if (!$this->getDB('isRemovedFile')) {
            if (file_exists($fileSQL) && file_exists($this->getDB('filePath'))) {
                if (unlink($fileSQL) && unlink($this->getDB('filePath'))) {
                    $result = true;
                    $this->setDB('isRemovedFile', true);
                    $this->setDB('step', "FINISH");
                }
            }
        } else $result = true;

        return json_encode(array(
            'result' => $result,
        ));
    }

    public function ajaxExtractFile()
    {
        $result = false;
        if (!$this->getDB('isExtractFIle')) {
            $zip = new ZipArchive;
            if ($zip->open($this->getDB('filePath')) === TRUE) {
                $zip->extractTo($this->rootDir);
                $zip->close();
                $result = true;
                $this->setDB('isExtractFIle', true);

            }
        } else $result = true;

        return json_encode(array(
            'result' => $result,
        ));
    }

    public function ajaxDBImport()
    {
        $result = false;
        $DS = DIRECTORY_SEPARATOR;
        $fileSQL = $this->rootDir . $DS . 'adman.sql';
        if (!$this->getDB('isDBImport')) {
            if (file_exists($fileSQL)) {
                $sql = file_get_contents($fileSQL);
                extract($this->getDB('database'));
                $mysqli = new mysqli($host, $login, $password, $db);
                if ($mysqli->multi_query($sql)) {
                    $result = true;
                    $this->setDB('isDBImport', true);
                }
            }
        } else $result = true;

        return json_encode(array(
            'result' => $result,
        ));
    }

    private function stepCheckCriticalServer()
    {

        $typeCheck = isset($_GET['check_all']) ? 'check_all' : false;

        switch ($typeCheck) {
            case 'check_all':
                $checkLogFiles = $this->viewFailLogFails($this->checkFailsLogs());
                $configuration = $this->viewFailCheck(
                    $this->configurationCheck(),
                    $this->dirList('fail'), $typeCheck);
                $integrity = $this->viewFailCheckIntegrity($this->checkIntegrity());
                $websitesIntegrity = $this->viewFailCheckWebsitesIntegrity($this->checkWebsitesIntegrity());
                $content = $configuration . $this->viewFailAdditional($this->additionalCheck()) . $integrity . $checkLogFiles . $websitesIntegrity;
                break;
            default:
                $conf = (count($this->configurationCheck(true))) ? $this->configurationCheck(true) : array();
                $dirList = (count($conf)) ? array() : $this->dirList('fail');
                $configuration = $this->viewFailCheck($conf, $dirList);
                $content = $configuration .$this->viewFailAdditional($this->additionalCheck());
        }

        if(!$content){
            $this->setDB('step', "INSTALL");
            $this->reload();
        }

        return $content. $this->viewBTN("", "Repeat the check");

    }

    private function checkPassword($password)
    {
        $url = $this->urlService['passwordCheck'];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array('password' => $password));
        $content = curl_exec($curl);
        curl_close($curl);

        return is_array($res = json_decode($content, true)) ? $res : false;
    }


    private function disablingPassword($password)
    {
        $url = $this->urlService['disablingKey'];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, array('password' => $password));
        $content = curl_exec($curl);
        curl_close($curl);
        return is_array($res = json_decode($content, true)) ? $res : false;
    }

    private function installationEnd()
    {
        return $this->viewInstallationEnd($this->rootDir, $this->getDB('paths')['php']);
    }


    private function stepInstall()
    {
        return $this->stepViewInstall();
    }

    private function checkPermissions()
    {

        if (!is_writable($this->rootDir)) {
            throw new Exception('Errors With File Permissions');
        }
    }


    private function stepSetUpSettings()
    {


        $data = count($_POST) ? $_POST : $this->getDB('all');

        $errors = array();

        $isCheck = false;

        if (isset($_POST['database']['host']) &&
            isset($_POST['database']['login']) &&
            isset($_POST['database']['password']) &&
            isset($_POST['database']['db'])) {
            extract($_POST['database']);
            $conn = @mysqli_connect($host, $login, $password, $db);
            if (mysqli_connect_error()) {
                $errors['db'] = 'Connection failed (' . mysqli_connect_errno() . ') ';
            } else {
                if ($checkPrivileges = $conn->query('SHOW GRANTS FOR CURRENT_USER')->fetch_all()) {
                    if (isset($checkPrivileges[0][0])) {
                        $string = strstr(str_replace('GRANT ', '', $checkPrivileges[0][0]), ' ON *.*', true);
                        $arrPrivileges = explode(', ', $string);
                        $arrClear = array_map('trim', $arrPrivileges);
                        if (count($diff = array_diff($arrClear, $this->privilegesDB))) {
                            $errors['db'] = 'User privileges failed (' . implode(",", $diff) . ') ';
                        }

                    }

                }
            }
        }

        if (isset($_POST['paths']['http']) && isset($_POST['paths']['php']) && isset($_POST['paths']['nice'])) {
            extract($_POST['paths']);
            if (!shell_exec($nice . ' --version')) {
                $errors['nice'] = 'Nice not found';
            }
            if (!shell_exec($php . ' -v')) {
                $errors['php'] = 'PHP not found';
            }

            if ($this->check_http_status($http) != 200) {
                $errors['http'] = 'Http not found';
            }


        }

        if (!count($errors) && isset($_POST['paths']['http']) && isset($_POST['database']['password'])) {

            $isCheck = true;
            if ($_POST['type'] == "save") {
                $this->setDB('database', $_POST['database']);
                $this->setDB('paths', $_POST['paths']);
                $this->setDB('step', "CHECK");
                $this->reload();
            }

        }


        return $this->stepViewSetUpSettings($data, $errors, $isCheck);
    }


    private function stepDownloadFile()
    {
        return $this->stepViewDownloadFile();
    }

    private function check_http_status($url)
    {
        $user_agent = 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0)';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $page = curl_exec($ch);

        $err = curl_error($ch);
        if (!empty($err))
            return $err;

        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $httpcode;
    }

    private function adman_encoded($word)
    {
        return base64_encode($word);
    }

    private function adman_decoded($data)
    {
        return base64_decode($data);
    }

    private function stepPassword()
    {
        if (isset($_POST['password'])) {
            if ($result = $this->checkPassword($this->adman_encoded($_POST['password']))) {
                if ($result['result']) {
                    $this->setDB("password", $this->adman_encoded($_POST['password']));
                    $this->setDB("step", "DOWNLOADFILE");
                    $this->reload();
                } else {
                    return $this->stepViewPassword("The password is incorrect");
                }
            } else {
                return $this->stepViewPassword("Server response error");
            }

        }

        return $this->stepViewPassword("");
    }

    private function download($url)
    {
        $name = basename($url);
        $DS = DIRECTORY_SEPARATOR;
        $ch = curl_init();
        $filePath = $this->rootDir . $DS . $this->tempDir . $DS . $name;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_PROGRESSFUNCTION, array($this, 'progress'));
        curl_setopt($ch, CURLOPT_NOPROGRESS, false);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "");
        $fp = fopen($filePath, 'wb');
        $this->setDB('filePath', $filePath);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        return $info["http_code"] == 200 ? true : false;
    }

    function progress($resource, $download_size, $downloaded, $upload_size, $uploaded)
    {
        $pr = 0;
        if ($download_size > 0)
            $pr = $downloaded / $download_size * 100;

        if ($pr)
            $this->setDB("progressDownloadFile", $pr);


    }

    private function reload()
    {
        header('Location: /' . basename(__FILE__));
    }

    private function checkDBFile()
    {
        $DS = DIRECTORY_SEPARATOR;
        $this->dbPath = $this->rootDir . $DS . $this->tempDir . $DS . $this->dbFile;
        if (!file_exists($this->dbPath)) {
            if (!is_dir($this->rootDir . $DS . $this->tempDir)) mkdir($this->rootDir . $DS . $this->tempDir, 0666);
            $this->databaseShema['paths']['http'] = $this->publicUrl;
            if (!file_put_contents($this->dbPath, $this->adman_encoded(serialize($this->databaseShema)))) {
                throw new Exception('Error creating and writing the database file');
            }
        }

        return $this->dbPath;
    }

    private function setDB($key, $value)
    {
        $db = unserialize($this->adman_decoded(file_get_contents($this->dbPath)));
        if ((isset($db[$key]))) {
            $db[$key] = $value;
            if ($this->saveDB($db))
                return true;
        }
        return false;
    }

    private function getDB($key)
    {
        $db = unserialize($this->adman_decoded(file_get_contents($this->dbPath)));

        if ($key === "all")
            return $db;

        return (isset($db[$key])) ? $db[$key] : false;
    }

    private function saveDB($data)
    {
        if (!file_put_contents($this->dbPath, $this->adman_encoded(serialize($data)))) {
            throw new Exception('Error creating and writing the database file');
        }
        return true;
    }
    private function checkIntegrity()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->urlService['integrityCheck']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        $content = curl_exec($curl);
        curl_close($curl);
        return is_array($res = json_decode($content, true)) ? $res : false;
    }

    private function checkWebsitesIntegrity()
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->urlService['websitesIntegrityCheck']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        $content = curl_exec($curl);
        curl_close($curl);
        return is_array($res = json_decode($content, true)) ? $res : false;
    }


    private function checkFailsLogs()
    {
        $res = $error = array();
        try {
            $it = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->rootDir . $this->cronLogsDir));
            $it->setFlags(RecursiveDirectoryIterator::SKIP_DOTS);
            while ($it->valid()) {
                if ($it->isFile())
                    $res[basename($it->getPathname())] = array(
                        'path' => realpath($it->getPathname()),
                        'is_writable' => is_writable($it->getPathname()),
                        'fileperms' => substr(sprintf('%o', fileperms($it->getPathname())), -4),
                        'name' => basename($it->getPathname()),
                        'filemtime' => filemtime($it->getPathname())
                    );
                $it->next();
            }
        } catch (Exception $e) {
            $error[] = $e;
        }
        $result = array();
        foreach ($this->checkLogFiles as $item) {
            $result[] = array(
                'path' => isset($res[$item]['path']) ? $res[$item]['path'] : null,
                'name' => $item,
                'is_exist' => (bool)in_array($item, array_keys($res)),
                'uptime' => isset($res[$item]['filemtime']) ? ((bool)in_array($item, array_keys($res)) ? $res[$item]['filemtime'] : false) : false,
                'check_time' => isset($res[$item]['filemtime']) ? ((time() - $res[$item]['filemtime']) <= $this->checkUpTimeLogFile ? true : false) : false
            );
        }

        return $result;
    }

    private function getPHPExecutableFromPath()
    {
        $paths = explode(PATH_SEPARATOR, getenv('PATH'));
        foreach ($paths as $path) {
            if (strstr($path, 'php.exe') && isset($_SERVER["WINDIR"]) && file_exists($path) && is_file($path)) {
                return $path;
            } else {
                $php_executable = $path . DIRECTORY_SEPARATOR . "php" . (isset($_SERVER["WINDIR"]) ? ".exe" : "");
                if (file_exists($php_executable) && is_file($php_executable)) {
                    return $php_executable;
                }
            }
        }

        if (getenv('PHPBIN'))
            return getenv('PHPBIN');

        if (PHP_BINARY)
            return PHP_BINARY;

        return false;
    }

    private function dirList($type = 'all')
    {
        $res = $error = array();
        try {
            $directory = new RecursiveDirectoryIterator($this->rootDir);
            $it = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::SELF_FIRST);
            while ($it->valid()) {
                if (!array_intersect($this->excludeDirs, explode(DIRECTORY_SEPARATOR, $it->getPathname()))) {
                    if ($type == 'all') {
                        $res[] = array(
                            'path' => realpath($it->getPathname()),
                            'is_writable' => is_writable($it->getPathname()),
                            'fileperms' => substr(sprintf('%o', fileperms($it->getPathname())), -4),
                            'is_dir' => $it->isDir(),
                        );
                    }
                    if ($type == 'fail') {
                        if (!is_writable($it->getPathname()))
                            $res[] = array(
                                'path' => realpath($it->getPathname()),
                                'is_writable' => is_writable($it->getPathname()),
                                'fileperms' => substr(sprintf('%o', fileperms($it->getPathname())), -4),
                                'is_dir' => $it->isDir()
                            );
                    }
                }
                $it->next();
            }
        } catch (Exception $e) {
            $error = $e;
        }
        return array_unique($res, SORT_REGULAR);
    }

    private function checkTimeOut()
    {
        if ($terminateTimeout = ini_get('request_terminate_timeout')) {
            if ($terminateTimeout >= 1800)
                return true;
        } elseif ($executionTimeout = ini_get('max_execution_time')) {
            if ($executionTimeout >= 1800)
                return true;
        }
        return false;
    }

    private function checkPublicUrl($type = '')
    {
        if ($type == 'rand')
            return $this->check_http_status($this->publicUrl . '/r/test' . rand(1, 99)) == 200 ? true : false;

        return $this->check_http_status($this->publicUrl . '/r/url.php') == 200 ? true : false;
    }

    private function additionalCheck()
    {
        $result = array();
        $result[] = array('check' => 'timeout', 'isPass' => $this->checkTimeOut());
        $result[] = array('check' => 'publicAccessUrl', 'isPass' => $this->checkPublicUrl());
        $result[] = array('check' => 'checkRedirected', 'isPass' => $this->checkPublicUrl('rand'));
        return $result;

    }

    private function configurationCheck($isPass = false)
    {
        $result = array();
        $result[] = array('check' => 'mysql', 'isPass' => (bool)function_exists('mysqli_connect'));
        $result[] = array('check' => 'phpversion', 'isPass' => (bool)version_compare(phpversion(), $this->minPHPVersion, '>='));
        $result[] = array('check' => 'safe_mode', 'isPass' => !ini_get('safe_mode') ? true : false);
        $result[] = array('check' => 'short_open_tag', 'isPass' => ini_get('short_open_tag') ? true : false);
        $result[] = array('check' => 'memory_limit', 'isPass' => (int)ini_get('memory_limit') >= $this->minMemoryLimit ? true : false);
        $result[] = array('check' => 'exec', 'isPass' => (bool)function_exists('exec'));
        $result[] = array('check' => 'shell_exec', 'isPass' => (bool)function_exists('shell_exec'));
        if (function_exists('shell_exec'))
            $result[] = array('check' => 'Gzip', 'isPass' => shell_exec('gzip -V') ? true : false);
        if (function_exists('shell_exec'))
            $result[] = array('check' => 'nice', 'isPass' => shell_exec('nice --version') ? true : false);
        if (function_exists('shell_exec'))
            $result[] = array('check' => 'mysqldump', 'isPass' => shell_exec('mysqldump --version') ? true : false);
        $result[] = array('check' => 'popen', 'isPass' => function_exists('popen') ? true : false);
        foreach ($this->extensionsCheck as $extension) {
            $result[] = array('check' => $extension, 'isPass' => (bool)extension_loaded($extension) ? true : false);
        }
        if ($isPass) {
            $isPass = $result;
            $result = array();
            foreach ($isPass as $item)
                if (!$item['isPass'])
                    $result[] = $item;
        }
        return $result;
    }

}


class ViewAdmanInstall
{


    public function viewBTN($url = "", $text = "")
    {
        return <<<VIEW
<a class="btn btn-primary" id="final"  href="{$url}">{$text} </a>
VIEW;
    }

    public function viewInstallationEnd($rootDir, $pathPHPBin)
    {

        $html = "";
        $ds = DIRECTORY_SEPARATOR;
        $helperArr = array(
            'core_cron' => "{$ds}cron.php",
            'test_db_response' => "{$ds}api{$ds}test_db_response.php loop=1",
            'test_db_response_time' => "{$ds}api{$ds}test_db_response_time.php loop=1"
        );
        $defaultPhpPath = "&lt;PATH_TO_PHP&gt;";
        foreach ($helperArr as $key => $item) {

            $html .= '<p>* * * * * ' . ($pathPHPBin ? $pathPHPBin : $defaultPhpPath) . '  ' . $rootDir . $ds . ($key) . '</p>';


        }
        $html = '<div class="name-section">Cron setup</div><ul class="fail-list-file">' . $html . "</ul>";

        return <<<VIEW
        <div style="color:#0c850c;" class="name-section">Congratulations</div>
<p style="color:#0c850c;">TM Ad Manager was installed successfully</p>
{$html}
<a class="btn btn-primary" id="final" onclick="final()" href="#">Remove the installer and go to admin</a>
VIEW;

    }

    public function stepViewInstall()
    {
        return <<<VIEW
<div class="name-section">Unpacking the archive</div>
<div id="unpacking">Wait</div>

<div class="name-section">Database import</div>
<div id="dbimport">Wait</div>

<div class="name-section">Checking the installation</div>
<div id="checkinstall">Wait</div>

<div class="name-section">Removing temporary files</div>
<div id="cleartmp">Wait</div>
 <a class="btn btn-primary" id="nextStepEND" href="?congratulations">Next Step</a>
VIEW;

    }

    public function stepViewSetUpSettings($data = array(), $errors = array(), $isCheck = false)
    {

        extract($data['database']);

        extract($data['paths']);

        $error_db = isset($errors['db']) ? $errors['db'] : null;

        $error_http = isset($errors['http']) ? $errors['http'] : null;

        $error_php = isset($errors['php']) ? $errors['php'] : null;

        $error_nice = isset($errors['nice']) ? $errors['nice'] : null;

        $isCheck = $isCheck ? "<p style=\"color:#0c850c;\">Settings data valid</p>" : "";


        return <<<VIEW
<div class="name-section">Settings DataBase</div>

        <form autocomplete="off" action="" method="post">
        <div class="form-input">
             <input type="text" name="database[host]" placeholder="DataBase Host" class="form-control" value="{$host}" required>
        </div>
        <div class="form-input">
             <input type="text" name="database[login]" placeholder="DataBase User" class="form-control" value="{$login}" required>
        </div>
        <div class="form-input">
             <input type="password" name="database[password]" placeholder="DataBase Password" class="form-control" required>
        </div>
        <div class="form-input">
             <input type="text" name="database[db]" placeholder="DataBase Name" class="form-control" value="{$db}" required>
        </div>
       <div class="error-setup"> {$error_db}</div>

        <div class="name-section">Settings Paths</div>
        
        <div class="form-input">
             <input type="text" name="paths[http]" placeholder="HTTP Path" class="form-control" value="{$http}" required>
             <div class="error-setup">{$error_http}</div>
        </div>
        
        <div class="form-input">
             <input type="text" name="paths[php]" placeholder="PHP Path" class="form-control" value="{$php}" required>
            <div class="error-setup"> {$error_php}</div>
        </div>
        
        <div class="form-input">
             <input type="text" name="paths[nice]" placeholder="Nice Path" class="form-control" value="{$nice}" required>
             <div class="error-setup"> {$error_nice}</div>
        </div>
         {$isCheck}
        <button class="btn btn-primary" name="type" value="save" type="submit">Save</button>
        
        <button class="btn btn-primary" name="type" value="check" type="submit">Check Settings</button>
        
       
        
        </form>
VIEW;

    }


    public function stepViewDownloadFile()
    {
        return <<<VIEW
<div class="name-section">Download file</div>
<div id="progress-bar-file">
<div id="myBar">0%</div>
</div>
<div class="name-section">Checking the archive</div>
<div id="checksumStatus">Wait</div>

 <a class="btn btn-primary" id="nextStepPBF" href="">Next Step</a>
 <a class="btn btn-primary" id="reloadBTN" href="">Reload</a>

VIEW;


    }

    public function stepViewPassword($errorView)
    {

        return '
        <div class="name-section">Check password</div>
        <div class="error-block">' . $errorView . '</div>
        <form autocomplete="off" action="" method="post">
        <div class="form-input">
        <input type="password" name="password" placeholder="Password" class="form-control" required>
        </div>
        <button class="btn btn-primary" type="submit">Submit</button>
        </form>

';

    }
}


class ViewAdman extends ViewAdmanInstall
{

    private $errorDescriptionArray = array(
        'curl' => 'PHP extension {ext} must be installed',
        'mysqli' => 'PHP extension {ext} must be installed',
        'date' => 'PHP extension {ext} must be installed',
        'zip' => 'PHP extension {ext} must be installed',
        'hash' => 'PHP extension {ext} must be installed',
        'json' => 'PHP extension {ext} must be installed',
        'mbstring' => 'PHP extension {ext} must be installed',
        'pcre' => 'PHP extension {ext} must be installed',
        'simplexml' => 'PHP extension {ext} must be installed',
        'openssl' => 'PHP extension {ext} must be installed',
        'IonCube Loader' => 'PHP extension {ext} must be installed',
        'mysql' => 'Mysql function is <strong>disabled</strong>',
        'phpversion' => 'You need<strong> PHP 5.6.0</strong> (or greater)',
        'safe_mode' => 'PHP safe_mode is <strong>on</strong>',
        'short_open_tag' => 'PHP short_open_tag is <strong>off</strong>',
        'memory_limit' => 'PHP memory_limit must be at least <strong>256M</strong>',
        'exec' => 'Exec function is <strong>disabled</strong>',
        'shell_exec' => 'Shell_exec function is <strong>disabled</strong>',
        'Gzip' => 'Gzip command is <strong>disabled</strong>',
        'nice' => 'Nice command is <strong>disabled</strong>',
        'popen' => 'Popen function is <strong>disabled</strong>',
        'mysqldump' => 'Mysqldump command is <strong>disabled</strong>',
        'checkFileAndDirAdMan' => 'Error no permission to write the file',
        'timeout' => 'PHP timeout must be at least <strong>1800</strong>',
        'publicAccessUrl' => 'You need to open the public access to script /r/url.php',
        'checkRedirected' => 'All requests to folder /r/ must be redirected to /r/url.php this setting must be relevant to this htaccess rule: RewriteRule .* url.php?param=$0 [L]'
    );

    protected function errorDescription($ext = "")
    {
        return $this->errorDescriptionArray[$ext] ?
            str_replace("{ext}",
                "<a target='_blank' href='https://www.google.ru/search?q={$ext}+install+php'>{$ext}</a>",
                $this->errorDescriptionArray[$ext])
            : "Error {$ext} ";
    }

    protected function viewFailLogFails($data = array())
    {
        $html = "";
        $ds = DIRECTORY_SEPARATOR;
        $helperArr = array(
            'core_cron' => "{$ds}cron.php",
            'test_db_response' => "{$ds}api{$ds}test_db_response.php loop=1",
            'test_db_response_time' => "{$ds}api{$ds}test_db_response_time.php loop=1"
        );
        $defaultPhpPath = "&lt;PATH_TO_PHP&gt;";
        foreach ($data as $key => $item) {
            if (!$item['is_exist'] || !$item['check_time']) {
                $desc = '<p>* * * * * ' . ($this->pathPHPBin ? $this->pathPHPBin : $defaultPhpPath) . '  ' . $this->rootDir . '' . (isset($helperArr[$item['name']]) ? $helperArr[$item['name']] : "") . '</p>';
                $html .= '<li><div><b>' . $item['name'] . ':</b>' . $desc . '</div><span>' . ((!$item['is_exist']) ? "no file" : "file is out of date") . '</span></li>';
            }
        }
        return $html ? '<div class="name-section">Cron Check</div><ul class="fail-list-file">' . $html . "</ul>" : false;
    }

    protected function viewFailCheckWebsitesIntegrity($data)
    {
        if (gettype($data) != "boolean") {
            $html = "";
            foreach ($data as $item) {
                if (isset($item['error']) && isset($item['message'])) {
                    if ($item['error'])
                        $html .= '<li><div>' . $item['message'] . '</div></li>';
                }
            }
            return $html ? '<div class="name-section">Websites Integrity Check</div><ul class="fail-list">' . $html . '</ul>' : false;
        } else return "<div class=\"name-section\">Websites Integrity Check</div><li><div>Verification server is not available</div></li>";
    }

    protected function viewFailCheckIntegrity($data)
    {
        if (gettype($data) != "boolean") {
            $html = "";
            foreach ($data as $item) {
                if (isset($item['error']) && isset($item['message'])) {
                    if ($item['error'])
                        $html .= '<li><div>' . $item['message'] . '</div></li>';
                }
            }
            return $html ? '<div class="name-section">Integrity Check</div><ul class="fail-list">' . $html . '</ul>' : false;
        } else return "<div class=\"name-section\">Integrity Check</div><li><div>Verification server is not available</div></li>";
    }

    protected function viewFailAdditional($data = array())
    {
        $html = "";
        foreach ($data as $item) {
            if (!$item['isPass']) {
                $html .= '<li><div>' . $this->errorDescription($item['check']) . '</div></li>';
            }
        }
        return $html ? '<div class="name-section">Additional Server Requirements</div><ul class="fail-list">' . $html . '</ul>' : false;
    }

    protected function viewFailCheck($data = array(), $dataFile = array())
    {
        $html = $checkFile = "";
        foreach ($data as $item) {
            if (!$item['isPass']) {
                $html .= '<li><div>' . $this->errorDescription($item['check']) . '</div></li>';
            }
        }
        foreach ($dataFile as $file) {
            $checkFile .= '<li class="fail-list-file"><div title="' . $file['path'] . '">' . $file['path'] . '</div><span>' . $file['fileperms'] . '</span></li>';
        }

        $checkFile = $checkFile ? "<li><div>Error no permission to write the file:</div></li>" . $checkFile : false;
        $html = $html ? "<ul class='fail-list'>" . $html . $checkFile . "</ul>" : false;
        return $html ? '<div class="name-section">Сritical Server Requirements</div>' . $html : false;
    }


    public function body($content = "")
    {

        $file = basename(__FILE__);
        if (!$content)
            $content = '<p style="color:#0c850c;"><strong>Congratulations!</strong><br>Your server meets the requirements for TM Ad Manager.</p>';
        return <<<HTML
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0">
    <title>Adman</title>
    <link rel="shortcut icon" href="data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAuLi4AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAREAAAAAAAARERAAAAAREBEREQAAARAQAREREAARARAAERERARARAAABERARARAAAAARERARAAAAAAABEBAAAAAAAQARAAAAABERAAEQAAABERERABEAABERERAAAREAEAEREAABERAAABEQAAEREAAAEQAAABERAAEQAAAAABCP/wAAB/EAAAPlAACByQAAwJMAAOEnAADwTwAA/l8AAPs/AADDnwAAgM8AAAHjAABh4QAA8eEAAPPwAADn/QAA" />
<style>
 *
{
	margin:0;
	padding:0
}

li
{
	list-style-type:none;
}

li:last-child
{
    margin-bottom: 0;
}


ul
{
	margin-left:0;
	padding-left:0
}

.error-setup{
    color: #e36209;
    font-size: revert;
}

body
{
	margin:0;
	font-family:Roboto,sans-serif;
	font-size:.9rem;
	font-weight:400;
	line-height:1.6;
	color:#212529;
	text-align:left;
	background-color:#f4f3ef
}

.content
{
	max-width:1200px;
	margin:2% auto;
	padding:15px;
	background:#fff;
	box-shadow:0 6px 10px -4px rgba(0,0,0,.15)!important
}

.fail-title
{
	display:block;
	color:#e36209
}

.fail-list-file span,.fail-list-file div
{
	display:inline-block
}

.fail-list-file div
{
	width:80%;
	color:#949393;
	font-weight:100;
	text-overflow:ellipsis
}

.fail-list-file span
{
	font-weight:700;
	text-align:right;
	width:20%;
	color:#e36209
}

a
{
	color:#0089ff;
	border-bottom:2px dotted rgba(0,137,255,.3)!important;
	text-decoration-line:none
}

a:hover
{
	color:#0e36ff
}

.name-section
{
	font-size:14pt;
	font-weight:700;
	margin-top:15px;
	line-height: initial;
}

.name-section:first-child{
    margin: 0;
}

.form-control {
    display: block;
    width: 100%;
    padding: .375rem .75rem;
    font-size: 1rem;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    border-radius: .25rem;
}
.btn {
    display: inline-block;
    font-weight: 400;
    line-height: 1.5;
    color: #212529;
    text-align: center;
    text-decoration: none;
    vertical-align: middle;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    user-select: none;
    background-color: transparent;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    border-radius: .25rem;
}

.btn-group-lg>.btn, .btn-lg {
    padding: .5rem 1rem;
    font-size: 1.25rem;
    border-radius: .3rem;
}

.btn-primary {
    color: #fff;
    background-color: #0d6efd;
    border-color: #0d6efd;
}
[type=button], [type=reset], [type=submit], button {
    -webkit-appearance: button;
}
.form-input{
    width: 50%;
    margin: 0 0 15px;
}

#myBar {
  width: 10%;
  height: 30px;
  background-color: #4CAF50;
  text-align: center; 
  line-height: 30px; 
  color: white;
}

#progress-bar-file{
  width: 100%;
  background-color: grey;
}

#nextStepPBF, #nextStepEND, #reloadBTN{
display: none;
}

a:hover{
color:#fff;
}
</style>
</head>
<body>
<div class="content">
{$content}
</div>

<script type="application/javascript">
function XmlHttp() {
    let xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function ajax(param) {
    if (window.XMLHttpRequest) req = new XmlHttp();
    let send = "";
    for (let i in param.data) send += i + "=" + param.data[i] + "&";
    req.open("POST", param.url, true);
    req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    req.send(send);
    req.onreadystatechange = function () {
        if (req.readyState == 4 && req.status == 200) {
            if (param.success) param.success(req.responseText);
        }
    }
}

function final() {
    ajax({
        url: '/{$file}?ajaxFinal',
        success: function (data) {
            const res = JSON.parse(data)
            if (res.result) {
                location = res.redirect
            }
        }
    })
}

let pbf = document.getElementById('progress-bar-file'), progress = 0, bar = document.getElementById('myBar'),
    checksumStatus = document.getElementById('checksumStatus'), nextStepPBF = document.getElementById('nextStepPBF'),
    reloadBTN = document.getElementById('reloadBTN');


if (pbf) {
    ajax({
        url: '/{$file}?startDownload', success: function (data) {
            const res = JSON.parse(data)
            if (!res?.result) {
                pbf.innerText = "Fail"
                checksumStatus.innerText = "Fail"
                reloadBTN.style.display = "block"
            }
        }
    })

    let checkPG = setInterval(function () {
        ajax({
            url: '/{$file}?progressDownload',
            success: function (data) {
                progress = JSON.parse(data).progress
                if (progress) {
                    if (progress >= 10)
                        bar.style.width = progress + "%";
                    bar.innerHTML = progress + "%";
                }


                if (progress == 100) {
                    clearInterval(checkPG)
                    ajax({
                        url: '/{$file}?checksum',
                        success: function (data) {
                            let res = JSON.parse(data)
                            if (res.result) {
                                checksumStatus.innerText = "Sucsses"
                                nextStepPBF.style.display = "block"
                            } else {
                                checksumStatus.innerText = "Fail"
                                reloadBTN.style.display = "block"
                            }

                        }
                    })
                }

            }
        })
    }, 1000)

}


let unpacking = document.getElementById('unpacking'),
    dbimport = document.getElementById('dbimport'),
    checkinstall = document.getElementById('checkinstall'),
    cleartmp = document.getElementById('cleartmp'),
    nextStepEND = document.getElementById('nextStepEND');

if (unpacking && dbimport) {
    ajax({
        url: '/{$file}?ajaxExtractFile',
        success: function (data) {
            const res = JSON.parse(data);
            if (res.result) {
                unpacking.innerText = "Sucsses"
                ajax({
                    url: '/{$file}?ajaxDBImport',
                    success: function (data) {
                        const res = JSON.parse(data);
                        if (res.result) {
                            dbimport.innerText = "Sucsses"
                            ajax({
                                url: '/{$file}?ajaxCheckInstall',
                                success: function (data) {
                                    const res = JSON.parse(data);
                                    if (res.result) {
                                        checkinstall.innerText = "Sucsses"
                                        ajax({
                                            url: '/{$file}?ajaxRemovingFile',
                                            success: function (data) {
                                                const res = JSON.parse(data);
                                                if (res.result) {
                                                    cleartmp.innerText = "Sucsses"
                                                    nextStepEND.style.display = "block"

                                                } else {
                                                    cleartmp.innerText = "Fail"
                                                }
                                            }
                                        })

                                    } else {
                                        checkinstall.innerText = "Fail: " + res.error
                                    }
                                }
                            })

                        } else {
                            dbimport.innerText = "Fail"
                        }
                    }
                })
            } else {
                unpacking.innerText = "Fail"
            }
        }
    })
}



</script>
</body>
</html>

HTML;
    }

}


try {

    $app = new AdmanInstall();

    if (isset($_GET['startDownload'])) {
        echo $app->ajaxStartDownloadFile();
        exit;
    }

    if (isset($_GET['progressDownload'])) {
        echo $app->ajaxProgressFile();
        exit;
    }

    if (isset($_GET['checksum'])) {
        echo $app->ajaxCheckSumFile();
        exit;
    }

    if (isset($_GET['ajaxDBImport'])) {
        echo $app->ajaxDBImport();
        exit;
    }

    if (isset($_GET['ajaxCheckInstall'])) {
        echo $app->ajaxCheckInstall();
        exit;
    }

    if (isset($_GET['ajaxExtractFile'])) {
        echo $app->ajaxExtractFile();
        exit;
    }

    if (isset($_GET['ajaxRemovingFile'])) {
        echo $app->ajaxRemovingFile();
        exit;
    }

    if (isset($_GET['ajaxFinal'])) {
        echo $app->ajaxFinal();
        exit;
    }


    $app->init();
} catch (Exception $e) {
    $view = new ViewAdman();
    echo $view->body($e->getMessage());
}
